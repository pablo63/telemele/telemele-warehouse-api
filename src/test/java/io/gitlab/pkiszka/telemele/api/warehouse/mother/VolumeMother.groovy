package io.gitlab.pkiszka.telemele.api.warehouse.mother

import io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume

class VolumeMother {
    
    public static Volume ANY_VOLUME = Volume.of(BigDecimal.ONE, Unit.TON)
    
}
