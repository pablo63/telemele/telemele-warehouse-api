package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock.LockVolumeAttempt
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock.LockableVolumes
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock.LockedVolume
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release.LockedBatches
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release.LocketBatchesSeeker
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release.ReleasedBatches
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.Feedstocks
import spock.lang.Specification

import static io.gitlab.pkiszka.telemele.api.warehouse.mother.FeedstockTypeMother.ANY_FEEDSTOCK_TYPE
import static io.gitlab.pkiszka.telemele.api.warehouse.mother.OrderIdMother.ANY_ORDER_ID
import static io.gitlab.pkiszka.telemele.api.warehouse.mother.VolumeMother.ANY_VOLUME

class BatchesSpec extends Specification {
    
    Feedstocks feedstocks
    BatchRepository batchRepository
    LocketBatchesSeeker locketBatchesSeeker
    Batches batches
    
    def setup() {
        feedstocks = Mock()
        batchRepository = Mock()
        locketBatchesSeeker = Mock()
        batches = new Batches(feedstocks, batchRepository, locketBatchesSeeker)
    }
    
    def 'should attempt lock on given lockVolumeAttempts'() {
        given:
            LockVolumeAttempt lockVolumeAttempt = Mock()
            LockableVolumes lockableVolumes = Mock()
            lockVolumeAttempt.divideIntoLockableVolumes(batchRepository) >> lockableVolumes
        
        when:
            batches.attemptLockOn(lockVolumeAttempt)
        
        then:
            1 * lockableVolumes.lockAll(batchRepository)
    }
    
    def 'should release locked volumes'() {
        given:
            OrderId orderId = ANY_ORDER_ID
            LockedBatches lockedBatches = Mock()
            locketBatchesSeeker.seekForLockedBatches(orderId, batchRepository) >> lockedBatches
            ReleasedBatches releasedBatches = Mock()
            lockedBatches.releaseAll(batchRepository) >> releasedBatches
        
        when:
            def result = batches.releaseLockedVolumes(orderId)
        
        then:
            result == releasedBatches
    }
    
    def 'should get total locked volume by feedstockType and orderId'() {
        given:
            LockedVolume firstLockedVolume = LockedVolume.of(ANY_ORDER_ID, ANY_VOLUME)
            LockedVolume secondLockedVolume = LockedVolume.of(OrderId.of('different-order-id'), ANY_VOLUME)
            LockedVolume thirdLockedVolume = LockedVolume.of(ANY_ORDER_ID, ANY_VOLUME)
            Batch firstBatch = Mock()
            firstBatch.getLockedVolumes() >> [firstLockedVolume, secondLockedVolume]
            Batch secondBatch = Mock()
            secondBatch.getLockedVolumes() >> [firstLockedVolume, secondLockedVolume, thirdLockedVolume]
            batchRepository.findBatches(ANY_FEEDSTOCK_TYPE) >> [firstBatch, secondBatch]
        
        when:
            Volume totalLockedVolume = batches.getTotalLockedVolume(ANY_FEEDSTOCK_TYPE, ANY_ORDER_ID)
        
        then:
            totalLockedVolume.getVolume() == ANY_VOLUME.getVolume() * BigInteger.valueOf(3)
    }
    
//    def 'should return zero total volume, when there are no locked volumes'() {
//        given:
//
//        when:
//
//        then:
//
//    }
//
//    def 'should persist batch'() {
//        given:
//
//        when:
//
//        then:
//
//    }
//
//    def 'should throw exception, when feedstock verification failed'() {
//        given:
//
//        when:
//
//        then:
//
//    }
//
//    def 'should find by feedstocktype and batch statuses'() {
//        given:
//
//        when:
//
//        then:
//
//    }
//
//    def 'should throw exception when feedstock type does not exists'() {
//        given:
//
//        when:
//
//        then:
//
//    }
}
