package io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock

import io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit
import spock.lang.Specification
import spock.lang.Unroll

import static io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit.LITER
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit.METER
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit.TON

class UnitTest extends Specification {
    
    @Unroll
    def 'should be able to map from string to FeedstockUnit'() {
        expect:
            feedstockUnit == Unit.from(name)
        
        where:
            name    | feedstockUnit
            'meter' | METER
            'METER' | METER
            'Meter' | METER
            'Liter' | LITER
            'Ton'   | TON
    }
    
    def 'should throw exception when mapping from null to FeedstockUnit'() {
        when:
            Unit.from(null)
        
        then:
            RuntimeException exception = thrown()
            exception.message == 'Failed to map string to Unit. Given \'feedstockUnitName\' is null'
    }
    
    def 'should throw exception when mapping from not-known name to FeedstockUnit'() {
        given:
            String name = 'not-defined-name'
        
        when:
            Unit.from(name)
        
        then:
            RuntimeException exception = thrown()
            exception.message == "Failed to map name:'${name}' to Unit. Could not find match for given name."
    }
}
