package io.gitlab.pkiszka.telemele.api.warehouse.mother

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId

class OrderIdMother {
    
    public static OrderId ANY_ORDER_ID = OrderId.of('462ded68-3cb2-4efb-8c4b')
    
}
