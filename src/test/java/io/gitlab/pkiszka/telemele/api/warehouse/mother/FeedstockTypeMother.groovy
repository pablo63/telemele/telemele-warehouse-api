package io.gitlab.pkiszka.telemele.api.warehouse.mother

import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType

class FeedstockTypeMother {
    
    public static FeedstockType ANY_FEEDSTOCK_TYPE = FeedstockType.of('CU')
}
