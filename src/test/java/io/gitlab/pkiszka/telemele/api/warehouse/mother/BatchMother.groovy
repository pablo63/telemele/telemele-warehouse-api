package io.gitlab.pkiszka.telemele.api.warehouse.mother

import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch

import static io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch.of
import static io.gitlab.pkiszka.telemele.api.warehouse.mother.FeedstockTypeMother.ANY_FEEDSTOCK_TYPE
import static io.gitlab.pkiszka.telemele.api.warehouse.mother.VolumeMother.ANY_VOLUME

class BatchMother {
    
    public static Batch ANY_BATCH = of(ANY_VOLUME, ANY_FEEDSTOCK_TYPE)
    
}
