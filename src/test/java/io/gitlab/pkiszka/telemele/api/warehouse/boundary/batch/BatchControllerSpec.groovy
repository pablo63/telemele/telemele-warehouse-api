package io.gitlab.pkiszka.telemele.api.warehouse.boundary.batch

import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batches
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType
import io.gitlab.pkiszka.telemele.api.warehouse.infrastructure.WebSecurityConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import static groovy.json.JsonOutput.toJson
import static io.gitlab.pkiszka.telemele.api.warehouse.mother.BatchMother.ANY_BATCH
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest
@ContextConfiguration(classes = [WebSecurityConfiguration.class, BatchController.class])
class BatchControllerSpec extends Specification {
    
    @Autowired
    protected MockMvc mvc
    
    @Autowired
    Batches batches
    
    def 'should find batches when type and status are empty'() {
        given:
            List<BatchStatus> allAvailableStatuses = Arrays.asList(BatchStatus.values())
            batches.findBy(allAvailableStatuses) >> [ANY_BATCH]
        
        when:
            ResultActions resultActions = mvc.perform(
                    get('/api/batches')
            )
        
        then:
            resultActions.andExpect(status().isOk())
            assertResponsesBodyMatches(resultActions, ANY_BATCH)
    }
    
    def 'should find batches when type is given, but status is empty'() {
        given:
            List<BatchStatus> allAvailableStatuses = Arrays.asList(BatchStatus.values())
            batches.findBy(FeedstockType.of('CU'), allAvailableStatuses) >> [ANY_BATCH]
        
        when:
            ResultActions resultActions = mvc.perform(
                    get('/api/batches?type=CU')
            )
        
        then:
            resultActions.andExpect(status().isOk())
            assertResponsesBodyMatches(resultActions, ANY_BATCH)
    }
    
    def 'should find batches when status is given, but type is empty'() {
        given:
            batches.findBy([BatchStatus.from('available')]) >> [ANY_BATCH]
        
        when:
            ResultActions resultActions = mvc.perform(
                    get('/api/batches/?status=available')
            )
        
        then:
            resultActions.andExpect(status().isOk())
            assertResponsesBodyMatches(resultActions, ANY_BATCH)
    }
    
    def 'should find batches when both status and type are given'() {
        given:
            batches.findBy(FeedstockType.of('CU'), [BatchStatus.from('available')]) >> [ANY_BATCH]
        
        when:
            ResultActions resultActions = mvc.perform(
                    get('/api/batches/?status=available&type=CU')
            )
        
        then:
            resultActions.andExpect(status().isOk())
            assertResponsesBodyMatches(resultActions, ANY_BATCH)
    }
    
    def 'should save batch'() {
        given:
            Map request = [
                    availableVolume: [
                            volume: '1.00',
                            unit  : 'TON'
                    ],
                    feedstockType  : 'CU'
            ]
        
        when:
            ResultActions resultActions = mvc.perform(
                    post('/api/batches')
                            .contentType(APPLICATION_JSON)
                            .characterEncoding("utf-8")
                            .content(toJson(request))
            )
        
        then:
            resultActions.andExpect(status().isCreated())
            1 * batches.persistOne(_)
    }
    
    def 'should throw error when createBatchDocument is not valid'() {
        given:
            Map request = [
                    availableVolume: [
                            volume: '2',
                            unit  : 'TON'
                    ],
                    feedstockType  : 'CU'
            ]
        
        when:
            ResultActions resultActions = mvc.perform(
                    post('/api/batches')
                            .contentType(APPLICATION_JSON)
                            .characterEncoding("utf-8")
                            .content(toJson(request))
            )
        
        then:
            resultActions.andExpect(status().isBadRequest())
            0 * batches.persistOne(_)
    }
    
    void assertResponsesBodyMatches(ResultActions resultActions, Batch batch) {
        resultActions
                .andExpect(jsonPath('$[0].batchId').value(batch.batchId.toString()))
                .andExpect(jsonPath('$[0].availableVolume.volume').value(batch.availableVolume.volume.toString()))
                .andExpect(jsonPath('$[0].availableVolume.unit').value(batch.availableVolume.unit.toString()))
                .andExpect(jsonPath('$[0].feedstockType').value(batch.feedstockType.toString()))
    }
    
    @TestConfiguration
    static class StubConfig {
        DetachedMockFactory detachedMockFactory = new DetachedMockFactory()
        
        @Bean
        Batches batches() {
            return detachedMockFactory.Mock(Batches)
        }
    }
}
