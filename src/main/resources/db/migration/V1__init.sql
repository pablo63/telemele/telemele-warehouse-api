CREATE TABLE batch (
  batch_id varchar(64) NOT NULL,
  unit varchar(16) NOT NULL,
  available_volume NUMERIC(10, 2) NOT NULL,
  feedstock_type varchar(16) NOT NULL,
  status varchar(32) NOT NULL,
  PRIMARY KEY (batch_id)
);

CREATE TABLE locked_volume (
  batch_id varchar (64) NOT NULL,
  order_id varchar(64) NOT NULL,
  unit integer(16) NOT NULL,
  locked_volume NUMERIC(10, 2) NOT NULL,
  FOREIGN KEY (batch_id) REFERENCES batch(batch_id)
);

CREATE TABLE resource_booking (
  resource_booking_id varchar(64) NOT NULL,
  total_booked_volume NUMERIC(10, 2) NOT NULL,
  unit integer(16) NOT NULL,
  volume_to_book NUMERIC(10, 2) NOT NULL,
  creation_date DATE NOT NULL,
  feedstock_type varchar(16) NOT NULL,
  order_id varchar(64) NOT NULL,
  status varchar(32) NOT NULL,
  PRIMARY KEY (resource_booking_id)
);

