package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch;

import java.util.List;
import java.util.Optional;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;

public interface BatchRepository {

    // todo: this 3 find batch should be rather changed to one that receives a specification
    List<Batch> findBatches(FeedstockType feedstockType, List<BatchStatus> batchStatuses);

    List<Batch> findBatches(FeedstockType feedstockType);

    List<Batch> findBatches(List<BatchStatus> batchStatuses);

    Optional<Batch> findById(BatchId batchId);

    Batch getById(BatchId batchId);

    Batch saveBatch(Batch batch);
}
