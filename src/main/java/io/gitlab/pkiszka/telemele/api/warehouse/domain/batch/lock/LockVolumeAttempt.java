package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock;

import java.math.BigDecimal;
import java.util.List;

import com.google.common.collect.ImmutableList;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchRepository;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.google.common.base.Preconditions.checkArgument;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus.AVAILABLE;
import static java.math.BigDecimal.ZERO;
import static java.util.Objects.isNull;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@Getter
public class LockVolumeAttempt {

    private Volume volumeToLock;
    private FeedstockType feedstockType;
    private OrderId orderId;

    public static LockVolumeAttempt of(Volume volumeToLock, FeedstockType feedstockType, OrderId orderId) {
        checkArgument(!isNull(volumeToLock), "volumeToLock is null");
        checkArgument(!volumeToLock.isEmpty(), "volumeToLock is empty");
        checkArgument(!isNull(feedstockType), "feedstockType is null");
        checkArgument(!isNull(orderId), "orderId is null");
        return new LockVolumeAttempt(volumeToLock, feedstockType, orderId);
    }

    public LockableVolumes divideIntoLockableVolumes(BatchRepository batchRepository) {
        List<Batch> batches = batchRepository.findBatches(feedstockType, ImmutableList.of(AVAILABLE));
        return new VolumeToLockDivider(volumeToLock, orderId)
                .divideIntoLockableVolumes(batches)
                .getLockableVolumes();
    }

    private static class VolumeToLockDivider {
        private final Volume volumeToLock;
        @Getter
        private final LockableVolumes lockableVolumes;

        private VolumeToLockDivider(Volume volumeToLock, OrderId orderId) {
            this.volumeToLock = volumeToLock;
            this.lockableVolumes = LockableVolumes.empty(orderId);
        }

        VolumeToLockDivider divideIntoLockableVolumes(List<Batch> batches) {
            batches.stream()
                    .sorted(new Batch.AvailableVolumeComparator())
                    .forEach(batch -> {
                        if (hasRemainingVolumeToLock()) {
                            lockableVolumes.attachVolume(
                                    batch.getBatchId(),
                                    extractVolumeToLock(batch)
                            );
                        }
                    });
            return this;
        }

        private boolean hasRemainingVolumeToLock() {
            return getRemainingVolumeToLock().compareTo(ZERO) > 0;
        }

        private BigDecimal getRemainingVolumeToLock() {
            return this.volumeToLock.getVolume().subtract(lockableVolumes.getTotalVolumeValue());
        }

        private Volume extractVolumeToLock(Batch batch) {
            BigDecimal batchAvailableVolume = batch.getAvailableVolume().getVolume();
            BigDecimal volumeToLock = getRemainingVolumeToLock().min(batchAvailableVolume);
            return Volume.of(
                    volumeToLock,
                    batch.getAvailableVolume().getUnit()
            );
        }
    }
}
