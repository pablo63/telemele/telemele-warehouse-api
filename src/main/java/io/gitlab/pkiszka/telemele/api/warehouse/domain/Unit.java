package io.gitlab.pkiszka.telemele.api.warehouse.domain;

import java.util.stream.Stream;

import lombok.AllArgsConstructor;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.isNull;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
public enum Unit {
    TON("ton"),
    METER("meter"),
    LITER("liter"),
    UNSPECIFIED("-");

    private final String name;

    public static Unit from(String unitName) {
        checkArgument(!isNull(unitName), "Failed to map string to Unit. Given 'unitName' is null");
        return Stream.of(Unit.values())
                .filter(unit -> unit != UNSPECIFIED)
                .filter(unit -> unit.name.equalsIgnoreCase(unitName))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(
                        String.format("Failed to map name:'%s' to Unit. Could not find match for given name.", unitName)
                ));
    }

    public static Unit requireMatch(Unit unit1, Unit unit2) {
        checkArgument(!isNull(unit1), "requireMatch - unit1 is null");
        checkArgument(!isNull(unit2), "requireMatch - unit2 is null");
        if (unit1 != UNSPECIFIED && unit2 != UNSPECIFIED && unit1 != unit2) {
            throw new RuntimeException("Units mismatch");
        }
        return unit1 == UNSPECIFIED ? unit2 : unit1;
    }
}
