package io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource;

import java.util.List;
import org.springframework.stereotype.Service;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingStatus.unsettledOnly;
import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class ResourceBookings {

    private final ResourceBookingRepository resourceBookingRepository;

    public List<ResourceBooking> getUnsettledResourceBookings() {
        return resourceBookingRepository.findResourceBookings(unsettledOnly()).stream()
                .sorted(new ResourceBooking.CreationDateComparator())
                .peek(resourceBooking -> log.info("Found unsettledResourceBooking, orderId: {}", resourceBooking.getOrderId()))
                .collect(toList());
    }

    public void verifyBookingsExistFor(OrderId orderId) {
        List<ResourceBooking> resourceBookings = resourceBookingRepository.findResourceBookings(orderId);
        if (resourceBookings.isEmpty()) {
            throw ResourceBookingException.resourceBookingDoesNotExistForOrderId(orderId);
        }
    }

    public void updateStatusAfterPerformingLock(OrderId orderId, FeedstockType feedstockType, Volume totalLockedVolume) {
        ResourceBooking resourceBooking = resourceBookingRepository.getResourceBooking(feedstockType, orderId);
        resourceBooking.updateBookingAfterLockingVolumes(totalLockedVolume);
        resourceBookingRepository.saveOne(resourceBooking);
        log.info(
                "Updated ResourceBooking status after performing lock - resourceBookingId: {}, orderId: {}, feedstockType: {}",
                resourceBooking.getResourceBookingId(), orderId, feedstockType
        );
    }

    public void updateStatusAfterReleasingLock(OrderId orderId, FeedstockType feedstockType, Volume releasedVolume) {
        ResourceBooking resourceBooking = resourceBookingRepository.getResourceBooking(feedstockType, orderId);
        resourceBooking.updateBookingAfterReleasingVolumes(releasedVolume);
        resourceBookingRepository.saveOne(resourceBooking);
        log.info(
                "Updated ResourceBooking status after releasing lock - resourceBookingId: {}, orderId: {}, feedstockType: {}",
                resourceBooking.getResourceBookingId(), orderId, feedstockType
        );
    }
}
