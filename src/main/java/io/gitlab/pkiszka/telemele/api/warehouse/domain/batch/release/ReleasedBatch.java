package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(staticName = "of")
public class ReleasedBatch {
    private BatchId batchId;
    @Getter
    private FeedstockType feedstockType;
    @Getter
    private Volume releasedVolume;
}

