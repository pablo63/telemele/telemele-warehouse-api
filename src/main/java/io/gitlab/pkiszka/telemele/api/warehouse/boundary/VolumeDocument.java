package io.gitlab.pkiszka.telemele.api.warehouse.boundary;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@Getter
public class VolumeDocument {

    private static final VolumeDocument EMPTY_VOLUME_DOCUMENT = new VolumeDocument("0.00", "-");

    @NotNull
    @Pattern(regexp = "^[0-9]*.[0-9]{2}$", message = "Invalid 'volume' - value have to be numeric string with 2 decimal places")
    private String volume;

    @NotNull
    private String unit;

    public static VolumeDocument from(Volume volume) {
        return new VolumeDocument(
                volume.getVolume().toString(),
                volume.getUnit().name()
        );
    }

    public static VolumeDocument empty() {
        return EMPTY_VOLUME_DOCUMENT;
    }

    public Volume asVolume() {
        return Volume.of(
                new BigDecimal(volume),
                Unit.from(unit)
        );
    }
}
