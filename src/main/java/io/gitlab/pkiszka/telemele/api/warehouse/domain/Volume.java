package io.gitlab.pkiszka.telemele.api.warehouse.domain;

import java.math.BigDecimal;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static com.google.common.base.Preconditions.checkArgument;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit.UNSPECIFIED;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit.requireMatch;
import static java.lang.String.format;
import static java.util.Objects.isNull;
import static javax.persistence.EnumType.STRING;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Setter(value = PRIVATE)
@Getter
@Embeddable
@EqualsAndHashCode
public class Volume {

    public static Volume ZERO = new Volume(BigDecimal.ZERO, UNSPECIFIED);

    private BigDecimal volume;

    @Enumerated(STRING)
    private Unit unit;

    public static Volume of(BigDecimal volume, Unit unit) {
        checkArgument(!isNull(volume), "Failed to instantiate Volume - volume value is null");
        checkArgument(volume.compareTo(BigDecimal.ZERO) >= 0, "Failed to instantiate Volume - volume is negative: '%s'", volume.toString());
        checkArgument(!isNull(unit), "Failed to instantiate Volume - volume unit is null");
        return new Volume(volume, unit);
    }

    public Volume add(Volume volumeToAdd) {
        return Volume.of(
                this.volume.add(volumeToAdd.volume),
                requireMatch(unit, volumeToAdd.unit)
        );
    }

    public Volume subtract(Volume volumeToSubtract) {
        return Volume.of(
                this.volume.subtract(volumeToSubtract.volume),
                requireMatch(unit, volumeToSubtract.unit)
        );
    }

    public boolean isGreaterThan(Volume volumeToCompare) {
        requireMatch(unit, volumeToCompare.unit);
        return this.volume.compareTo(volumeToCompare.volume) > 0;
    }

    @Transient
    public boolean isEmpty() {
        return this.volume.compareTo(BigDecimal.ZERO) == 0;
    }

    @Override
    public String toString() {
        return format("%s %s", volume, unit);
    }
}
