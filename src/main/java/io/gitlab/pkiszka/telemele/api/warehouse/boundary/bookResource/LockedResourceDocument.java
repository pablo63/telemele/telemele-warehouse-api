package io.gitlab.pkiszka.telemele.api.warehouse.boundary.bookResource;

import io.gitlab.pkiszka.telemele.api.warehouse.application.lockedResources.LockedResource;
import io.gitlab.pkiszka.telemele.api.warehouse.boundary.VolumeDocument;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@Getter
class LockedResourceDocument {

    private String batchId;
    private VolumeDocument totalLockedVolume;
    private String feedstockType;
    private VolumeDocument volumeToLock;

    public static LockedResourceDocument from(LockedResource lockedResource) {
        return new LockedResourceDocument(
                lockedResource.getBatchIdValue(),
                VolumeDocument.from(lockedResource.getTotalLockedVolume()),
                lockedResource.getFeedstockTypeValue(),
                VolumeDocument.from(lockedResource.getVolumeToLock())
        );
    }
}
