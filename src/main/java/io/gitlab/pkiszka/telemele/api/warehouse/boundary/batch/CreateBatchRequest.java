package io.gitlab.pkiszka.telemele.api.warehouse.boundary.batch;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import io.gitlab.pkiszka.telemele.api.warehouse.boundary.VolumeDocument;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@Getter
class CreateBatchRequest {

    @NotNull
    @Valid
    private VolumeDocument availableVolume;

    @NotNull
    private String feedstockType;

    Batch asBatch() {
        return Batch.of(availableVolume.asVolume(), FeedstockType.of(feedstockType));
    }
}
