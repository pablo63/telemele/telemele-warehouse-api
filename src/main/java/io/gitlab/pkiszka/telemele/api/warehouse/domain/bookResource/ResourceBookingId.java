package io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static java.util.Objects.requireNonNull;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Embeddable
@EqualsAndHashCode
@Setter(value = PRIVATE)
@Getter(value = PRIVATE)
public class ResourceBookingId implements Serializable {

    @JsonValue
    private String resourceBookingId;

    @JsonCreator
    // can this be private?
    public static ResourceBookingId of(String resourceBookingId) {
        return new ResourceBookingId(requireNonNull(resourceBookingId));
    }

    static ResourceBookingId generate() {
        return new ResourceBookingId(
                UUID.randomUUID().toString().replace("-", "").substring(0, 24)
        );
    }

    @Override
    public String toString() {
        return this.resourceBookingId;
    }
}
