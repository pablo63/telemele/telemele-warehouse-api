package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Embeddable
@Getter
public class LockedVolume {

    private OrderId orderId;

    @AttributeOverrides({
            @AttributeOverride(name = "volume", column = @Column(name = "lockedVolume"))
    })
    private Volume volume;

    public static LockedVolume of(OrderId orderId, Volume volume) {
        return new LockedVolume(orderId, volume);
    }

    public boolean isOfOrderId(OrderId orderId) {
        return this.orderId.equals(orderId);
    }
}
