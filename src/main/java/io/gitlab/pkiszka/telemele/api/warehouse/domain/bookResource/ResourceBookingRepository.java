package io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource;

import java.util.List;
import java.util.Optional;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;

public interface ResourceBookingRepository {

    void saveOne(ResourceBooking resourceBooking);

    void saveMany(List<ResourceBooking> resourceBookings);

    List<ResourceBooking> findResourceBookings(List<ResourceBookingStatus> resourceBookingStatuses);

    List<ResourceBooking> findResourceBookings(OrderId orderId);

    ResourceBooking getResourceBooking(FeedstockType feedstockType, OrderId orderId);

    Optional<ResourceBooking> findById(ResourceBookingId resourceBookingId);
}
