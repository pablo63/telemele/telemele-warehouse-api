package io.gitlab.pkiszka.telemele.api.warehouse.boundary.bookResource;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.pkiszka.telemele.api.warehouse.application.lockedResources.LockedResourcesResolver;
import io.gitlab.pkiszka.telemele.api.warehouse.application.resourcesBooking.ResourcesBookingSaga;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBooking;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingRepository;
import lombok.AllArgsConstructor;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.ACCEPTED;

@RestController
@RequestMapping("/api/orders/{order-id}")
@AllArgsConstructor
public class ResourceBookingController {

    private final ResourcesBookingSaga resourcesBookingSaga;
    private final ResourceBookingRepository resourceBookingRepository;
    private LockedResourcesResolver lockedResourcesResolver;

    @PostMapping(value = "/book-resources")
    @ResponseStatus(ACCEPTED)
    public AcceptedResourceBookingsDocument bookResources(@NotBlank @PathVariable("order-id") OrderId orderId,
                                                          @Valid @RequestBody BookResourceRequest bookResourceRequest) {
        List<ResourceBooking> resourceBookings = bookResourceRequest.asLockVolumeRequest(orderId);
        resourcesBookingSaga.acceptNewResourcesToBook(resourceBookings);
        return AcceptedResourceBookingsDocument.from(resourceBookings);
    }

    @PutMapping(value = "/release-resources")
    public void releaseResources(@NotBlank @PathVariable("order-id") OrderId orderId) {
        resourcesBookingSaga.releaseBookedResources(orderId);
    }

    @GetMapping(value = "/book-resources")
    public List<LockedResourceDocument> getBookedResources(@PathVariable("order-id") OrderId orderId) {
        return lockedResourcesResolver.getLockedResources(orderId).stream()
                .map(LockedResourceDocument::from)
                .collect(toList());
    }

    @GetMapping(value = "/book-resources-status")
    public ResourceBookingDocument getBookResourcesStatus(@PathVariable("order-id") OrderId orderId) {
        List<ResourceBooking> bookings = resourceBookingRepository.findResourceBookings(orderId);
        return ResourceBookingDocument.from(bookings);
    }
}
