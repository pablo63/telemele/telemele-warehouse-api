package io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource;

import java.math.BigDecimal;
import javax.persistence.Embeddable;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit.requireMatch;
import static java.math.BigDecimal.ZERO;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Embeddable
class BookingVolume {

    private BigDecimal volumeToBook;

    private BigDecimal totalBookedVolume;

    @Getter
    private Unit unit;

    static BookingVolume of(Volume volumeToBook) {
        return new BookingVolume(
                volumeToBook.getVolume(),
                ZERO,
                volumeToBook.getUnit()
        );
    }

    void setTotalBookedVolume(Volume totalBookedVolume) {
        requireMatch(unit, totalBookedVolume.getUnit());
        if (totalBookedVolume.getVolume().compareTo(volumeToBook) > 0) {
            throw new RuntimeException("Failed to set totalBookedVolume - totalBookedVolume is greater than volumeToBook");
        }
        this.totalBookedVolume = totalBookedVolume.getVolume();
    }

    void releaseVolume(Volume volumeToRelease) {
        requireMatch(unit, volumeToRelease.getUnit());
        totalBookedVolume = totalBookedVolume.subtract(volumeToRelease.getVolume());
    }

    boolean isSettled() {
        return volumeToBook.compareTo(totalBookedVolume) == 0;
    }

    boolean isTotalBookedValueEmpty() {
        return totalBookedVolume.compareTo(ZERO) == 0;
    }

    Volume getRemainingVolumeToBook() {
        return Volume.of(volumeToBook.subtract(totalBookedVolume), unit);
    }

    Volume getVolumeToBook() {
        return Volume.of(volumeToBook, unit);
    }

    Volume getTotalBookedVolume() {
        return Volume.of(totalBookedVolume, unit);
    }
}
