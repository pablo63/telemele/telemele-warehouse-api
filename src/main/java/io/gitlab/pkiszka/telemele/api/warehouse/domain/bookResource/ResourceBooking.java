package io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource;

import java.time.OffsetDateTime;
import java.util.Comparator;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Transient;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static com.google.common.base.Preconditions.checkArgument;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingStatus.CANCELLED;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingStatus.NOT_PROCESSED;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingStatus.PARTIALLY_CANCELLED;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingStatus.SETTLED;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingStatus.WAITING_FOR_AVAILABLE_RESOURCES;
import static java.util.Objects.isNull;
import static javax.persistence.EnumType.STRING;
import static lombok.AccessLevel.PRIVATE;

@Entity
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Getter
public class ResourceBooking {

    @EmbeddedId
    private ResourceBookingId resourceBookingId;

    @Embedded
    private BookingVolume bookingVolume;

    @Embedded
    private FeedstockType feedstockType;

    @Enumerated(STRING)
    private ResourceBookingStatus status;

    @Embedded
    private OrderId orderId;

    private OffsetDateTime creationDate;

    public static ResourceBooking from(Volume volumeToLock, FeedstockType feedstockType, OrderId orderId) {
        checkArgument(!isNull(volumeToLock), "Cannot instantiate ResourceBooking - volumeToLock is null");
        checkArgument(!isNull(feedstockType), "Cannot instantiate ResourceBooking - feedstockType is null");
        checkArgument(!isNull(orderId), "Cannot instantiate ResourceBooking - orderId is null");
        return new ResourceBooking(
                ResourceBookingId.generate(),
                BookingVolume.of(volumeToLock),
                feedstockType,
                NOT_PROCESSED,
                orderId,
                OffsetDateTime.now()
        );
    }

    @Transient
    public Unit getUnit() {
        return this.bookingVolume.getUnit();
    }

    @Transient
    public Volume getRemainingVolumeToBook() {
        return bookingVolume.getRemainingVolumeToBook();
    }

    @Transient
    public Volume getVolumeToLock() {
        return bookingVolume.getVolumeToBook();
    }

    @Transient
    public Volume getTotalLockedVolume() {
        return bookingVolume.getTotalBookedVolume();
    }

    void updateBookingAfterLockingVolumes(Volume totalLockedVolume) {
        if (status.isInCancellationProcess()) {
            throw ResourceBookingException.attemptToLockingResourcesAfterTriggeringCancellation(resourceBookingId);
        }
        bookingVolume.setTotalBookedVolume(totalLockedVolume);
        status = bookingVolume.isSettled() ? SETTLED : WAITING_FOR_AVAILABLE_RESOURCES;
    }

    void updateBookingAfterReleasingVolumes(Volume releasedVolume) {
        bookingVolume.releaseVolume(releasedVolume);
        status = bookingVolume.isTotalBookedValueEmpty()
                ? CANCELLED
                : PARTIALLY_CANCELLED;
    }

    public static class CreationDateComparator implements Comparator<ResourceBooking> {
        @Override
        public int compare(ResourceBooking resourceBooking1, ResourceBooking resourceBooking2) {
            checkArgument(!isNull(resourceBooking1), "cannot compare batches - first argument (resourceBooking1) is null");
            checkArgument(!isNull(resourceBooking2), "cannot compare batches - second argument (resourceBooking2) is null");
            return resourceBooking1.creationDate.compareTo(resourceBooking2.creationDate);
        }
    }
}
