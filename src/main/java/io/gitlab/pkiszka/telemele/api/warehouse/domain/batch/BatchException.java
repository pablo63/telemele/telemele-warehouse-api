package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;

import static java.lang.String.format;

class BatchException extends RuntimeException {

    private BatchException(String message, Object... formatArguments) {
        super(format(message, formatArguments));
    }

    static BatchException insufficientAvailableVolume(BatchId batchId, OrderId orderId, Volume volumeToLock) {
        return new BatchException(
                "Available volume is insufficient in batchId: '%s', for orderId: '%s'. Volume to lock: %s",
                batchId,
                orderId,
                volumeToLock.getVolume()
        );
    }

    static BatchException lockedVolumesForOrderIdNotFound(BatchId batchId, OrderId orderId) {
        return new BatchException(
                "Batch (batchId: '%s') does not contain any locked volume for orderId: '%s'", batchId, orderId
        );
    }
}
