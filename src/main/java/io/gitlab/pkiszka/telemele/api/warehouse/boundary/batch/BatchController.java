package io.gitlab.pkiszka.telemele.api.warehouse.boundary.batch;

import java.util.Arrays;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batches;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.AllArgsConstructor;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/batches")
@AllArgsConstructor
public class BatchController {

    private final Batches batches;

    @PostMapping
    @ResponseStatus(CREATED)
    public void saveBatch(@Valid @RequestBody CreateBatchRequest createBatchRequest) {
        Batch batch = createBatchRequest.asBatch();
        batches.persistOne(batch);
    }

    @GetMapping
    public List<BatchDocument> findBatches(@RequestParam(value = "type", required = false) FeedstockType feedstockType,
                                           @RequestParam(value = "status", defaultValue = "") List<String> batchStatusNames) throws InterruptedException {
        List<BatchStatus> batchStatuses = batchStatusNames.isEmpty()
                ? selectAllBatchStatuses()
                : extractRequestedBatchStatuses(batchStatusNames);
        List<Batch> batchesCollection = isNull(feedstockType)
                ? batches.findBy(batchStatuses)
                : batches.findBy(feedstockType, batchStatuses);
        return batchesCollection.stream()
                .map(BatchDocument::from)
                .collect(toList());
    }

    private List<BatchStatus> extractRequestedBatchStatuses(List<String> batchStatusNames) {
        return batchStatusNames.stream()
                .map(BatchStatus::from)
                .collect(toList());
    }

    private List<BatchStatus> selectAllBatchStatuses() {
        return Arrays.asList(BatchStatus.values());
    }
}
