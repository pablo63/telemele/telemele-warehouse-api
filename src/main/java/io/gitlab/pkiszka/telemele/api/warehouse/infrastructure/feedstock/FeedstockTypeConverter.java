package io.gitlab.pkiszka.telemele.api.warehouse.infrastructure.feedstock;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;

@Component
@ConfigurationPropertiesBinding
public class FeedstockTypeConverter implements Converter<String, FeedstockType> {

    @Override
    public FeedstockType convert(String feedstockTypeName) {
        return FeedstockType.of(feedstockTypeName);
    }
}
