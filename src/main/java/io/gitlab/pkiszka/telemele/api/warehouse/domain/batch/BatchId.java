package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch;


import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.isNull;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.isBlank;

@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Embeddable
@EqualsAndHashCode
public class BatchId implements Serializable {

    @JsonValue
    private String batchId;

    // todo: this method is never used - to be removed
    @JsonCreator
    public static BatchId of(String batchId) {
        checkArgument(!isNull(batchId), "Failed to instantiate BatchId, batchId value is null");
        checkArgument(!isBlank(batchId), "Failed to instantiate BatchId, batchId value is empty");
        return new BatchId(batchId);
    }

    public static BatchId generate() {
        return new BatchId(
                UUID.randomUUID().toString().replace("-", "").substring(0, 24)
        );
    }

    @Override
    public String toString() {
        return this.batchId;
    }
}
