package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableSet;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchRepository;
import lombok.AllArgsConstructor;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
public class LockedBatches {
    private final OrderId orderId;
    private final ImmutableSet<LockedBatch> lockedBatches;

    static LockedBatches of(OrderId orderId, Set<BatchId> batchIds) {
        Set<LockedBatch> lockedBatches = batchIds.stream()
                .map(LockedBatch::of)
                .collect(toSet());
        return new LockedBatches(orderId, ImmutableSet.copyOf(lockedBatches));
    }

    public ReleasedBatches releaseAll(BatchRepository batchRepository) {
        List<ReleasedBatch> releasedBatches = lockedBatches.stream()
                .map(lockedBatch -> lockedBatch.release(orderId, batchRepository))
                .collect(toList());
        return ReleasedBatches.of(releasedBatches);
    }
}
