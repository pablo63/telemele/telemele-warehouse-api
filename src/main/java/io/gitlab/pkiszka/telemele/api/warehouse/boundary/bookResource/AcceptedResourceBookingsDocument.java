package io.gitlab.pkiszka.telemele.api.warehouse.boundary.bookResource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonValue;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBooking;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
public class AcceptedResourceBookingsDocument {

    private static final String RESOURCE_BOOKING_LOCATION = "/api/lock-orders";

    @JsonValue
    private final List<AcceptedResourceBookingResponse> responses;

    public static AcceptedResourceBookingsDocument from(List<ResourceBooking> resourceBookings) {
        List<AcceptedResourceBookingResponse> responses = resourceBookings.stream()
                .map(AcceptedResourceBookingResponse::from)
                .collect(toList());
        return new AcceptedResourceBookingsDocument(responses);
    }

    @AllArgsConstructor(access = PRIVATE)
    @Getter
    private static class AcceptedResourceBookingResponse {
        private final String status = "ACCEPTED";

        private final String resourceBookingId;

        private final String acceptedResourceBookingLocation;

        private final String feedstockType;

        static AcceptedResourceBookingResponse from(ResourceBooking resourceBooking) {
            ResourceBookingId resourceBookingId = resourceBooking.getResourceBookingId();
            FeedstockType feedstockType = resourceBooking.getFeedstockType();
            return new AcceptedResourceBookingResponse(
                    resourceBookingId.toString(),
                    format("%s/%s", RESOURCE_BOOKING_LOCATION, resourceBookingId.toString()),
                    feedstockType.asString()
            );
        }
    }
}

