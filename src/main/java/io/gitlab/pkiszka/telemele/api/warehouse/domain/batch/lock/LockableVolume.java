package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchRepository;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor(staticName = "of")
@Slf4j
class LockableVolume {

    private final BatchId batchId;
    @Getter
    private final Volume volumeToLock;

    void lock(OrderId orderId, BatchRepository batchRepository) {
        Batch batchToLock = batchRepository.getById(batchId);
        batchToLock.lock(this.volumeToLock, orderId);
        batchRepository.saveBatch(batchToLock);
        log.info("Locked volume: {}, for batchId: {}, orderId: {}", volumeToLock.getVolume(), batchId, orderId);
    }

    boolean hasBatchId(BatchId batchId) {
        return this.batchId.equals(batchId);
    }
}
