package io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock;

import java.io.Serializable;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.isNull;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.isBlank;

@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Getter(value = PRIVATE)
@Embeddable
@EqualsAndHashCode
public class FeedstockType implements Serializable {

    @JsonValue
    private String feedstockType;

    @JsonCreator
    public static FeedstockType of(String feedstockType) {
        checkArgument(!isNull(feedstockType), "Failed to instantiate FeedstockType, feedstockType name is null");
        checkArgument(!isBlank(feedstockType), "Failed to instantiate FeedstockType, feedstockType name is empty");
        return new FeedstockType(feedstockType);
    }

    public String asString() {
        return this.feedstockType;
    }

    @Override
    public String toString() {
        return this.feedstockType;
    }
}
