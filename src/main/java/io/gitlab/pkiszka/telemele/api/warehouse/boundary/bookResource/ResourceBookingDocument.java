package io.gitlab.pkiszka.telemele.api.warehouse.boundary.bookResource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonValue;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBooking;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@Getter
public class ResourceBookingDocument {

    @JsonValue
    private final List<GetResourceBookingResponse> bookings;

    public static ResourceBookingDocument from(List<ResourceBooking> resourceBookings) {
        List<GetResourceBookingResponse> bookings = resourceBookings.stream()
                .map(GetResourceBookingResponse::from)
                .collect(toList());
        return new ResourceBookingDocument(bookings);
    }

    @AllArgsConstructor(access = PRIVATE)
    @Getter
    private static class GetResourceBookingResponse {
        private String id;
        private String feedstockType;
        private String bookingStatus;

        public static GetResourceBookingResponse from(ResourceBooking resourceBooking) {
            return new GetResourceBookingResponse(
                    resourceBooking.getResourceBookingId().toString(),
                    resourceBooking.getFeedstockType().asString(),
                    resourceBooking.getStatus().name()
            );
        }
    }
}
