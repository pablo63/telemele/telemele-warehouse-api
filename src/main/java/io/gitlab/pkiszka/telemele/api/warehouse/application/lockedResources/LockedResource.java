package io.gitlab.pkiszka.telemele.api.warehouse.application.lockedResources;

import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.Size;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock.LockedVolume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
public class LockedResource {

    private static final String SETTLED_STATUS = "SETTLED";
    private static final String WAITING_FOR_RESOURCES_STATUS = "WAITING_FOR_RESOURCES";

    @NonNull
    private BatchId batchId;

    @NonNull
    private FeedstockType feedstockType;

    @NonNull
    @Getter
    private Volume volumeToLock;

    @NonNull
    @Getter
    private Volume totalLockedVolume;

    @NonNull
    @Size(min = 1)
    @Getter
    private List<LockedVolume> lockedVolumes;

    public String getBatchIdValue() {
        return batchId.toString();
    }

    public String getFeedstockTypeValue() {
        return feedstockType.asString();
    }

    public String getStatus() {
        return volumeToLock.isGreaterThan(totalLockedVolume)
                ? WAITING_FOR_RESOURCES_STATUS
                : SETTLED_STATUS;
    }

    public static class BookedResourceBuilder {
        private BatchId batchId;
        private FeedstockType feedstockType;
        private Volume volumeToLock;
        private Volume totalLockedVolume;
        private @Size(min = 1) List<LockedVolume> lockedVolumes;

        LockedResource build() {
            if (totalLockedVolume.isGreaterThan(volumeToLock)) {
                throw new RuntimeException("BookedResourceBuilder exception - totalLockedVolume is greater than volumeToLock");
            }
            return new LockedResource(batchId, feedstockType, volumeToLock, totalLockedVolume, lockedVolumes);
        }
    }
}
