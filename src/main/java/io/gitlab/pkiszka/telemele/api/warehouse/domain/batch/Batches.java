package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch;

import java.util.List;
import org.springframework.stereotype.Service;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock.LockVolumeAttempt;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock.LockableVolumes;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock.LockedVolume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release.LockedBatches;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release.LocketBatchesSeeker;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release.ReleasedBatches;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.Feedstocks;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Service
@Slf4j
public class Batches {

    private final Feedstocks feedstocks;

    private final BatchRepository batchRepository;

    private final LocketBatchesSeeker locketBatchesSeeker;

    public void attemptLockOn(LockVolumeAttempt lockVolumeAttempt) {
        LockableVolumes lockableVolumes = lockVolumeAttempt.divideIntoLockableVolumes(batchRepository);
        lockableVolumes.lockAll(batchRepository);
    }

    public ReleasedBatches releaseLockedVolumes(OrderId orderId) {
        LockedBatches lockedBatches = locketBatchesSeeker.seekForLockedBatches(orderId, batchRepository);
        return lockedBatches.releaseAll(batchRepository);
    }

    public Volume getTotalLockedVolume(FeedstockType feedstockType, OrderId orderId) {
        return batchRepository.findBatches(feedstockType).stream()
                .flatMap(batch -> batch.getLockedVolumes().stream())
                .filter(lockedVolume -> lockedVolume.isOfOrderId(orderId))
                .map(LockedVolume::getVolume)
                .reduce(Volume.ZERO, Volume::add);
    }

    public void persistOne(Batch batch) {
        feedstocks.verifyMatch(batch.getFeedstockType(), batch.getAvailableVolume().getUnit());
        batchRepository.saveBatch(batch);
        log.info("Persisted batch with id: {}", batch.getBatchId());
    }

    public List<Batch> findBy(FeedstockType feedstockType, List<BatchStatus> batchStatuses) {
        feedstocks.verifyExists(feedstockType);
        return batchRepository.findBatches(feedstockType, batchStatuses);
    }

    public List<Batch> findBy(List<BatchStatus> batchStatuses) {
        return batchRepository.findBatches(batchStatuses);
    }
}
