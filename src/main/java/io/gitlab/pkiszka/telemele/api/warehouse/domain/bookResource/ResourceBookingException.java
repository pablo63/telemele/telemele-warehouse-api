package io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;

import static java.lang.String.format;

class ResourceBookingException extends RuntimeException {

    private ResourceBookingException(String message, Object... formatArguments) {
        super(format(message, formatArguments));
    }

    static ResourceBookingException resourceBookingDoesNotExistForOrderId(OrderId orderId) {
        return new ResourceBookingException(
                "Resource booking does not exist for given orderId: '%s'", orderId
        );
    }

    static ResourceBookingException releasedVolumeMismatch(ResourceBookingId resourceBookingId, Volume releasedVolume, Volume totalBookedVolume) {
        return new ResourceBookingException(
                "Failed to close ResourceBooking, id: '%s'. Released volume: '%s' does not equal total booked volume: '%s'",
                resourceBookingId, releasedVolume, totalBookedVolume
        );
    }

    static ResourceBookingException attemptToLockingResourcesAfterTriggeringCancellation(ResourceBookingId resourceBookingId) {
        return new ResourceBookingException("IllegalState - attempted to lock resource, after triggering cancellation process");
    }
}
