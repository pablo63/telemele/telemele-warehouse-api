package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release;

import java.util.List;

import com.google.common.collect.ImmutableList;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
public class ReleasedBatches {
    @Getter
    private ImmutableList<ReleasedBatch> releasedBatches;

    public static ReleasedBatches of(List<ReleasedBatch> releasedBatches) {
        return new ReleasedBatches(ImmutableList.copyOf(releasedBatches));
    }
}
