package io.gitlab.pkiszka.telemele.api.warehouse.infrastructure.feedstock;

import java.util.List;
import java.util.function.Predicate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Repository;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.Feedstocks;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static java.lang.String.format;

@Repository
@ConfigurationProperties(prefix = "telemele")
@Setter
public class ConfigBasedFeedstocks implements Feedstocks {

    private List<Feedstock> feedstocks;

    @Override
    public void verifyMatch(FeedstockType feedstockType, Unit unit) {
        feedstocks.stream()
                .filter(matchesType(feedstockType))
                .filter(matchesUnit(unit))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(
                        format("Not supported Feedstock: type: %s, unit: %s", feedstockType, unit)
                ));
    }

    @Override
    public void verifyExists(FeedstockType feedstockType) {
        feedstocks.stream()
                .filter(matchesType(feedstockType))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(
                        format("Not supported FeedstockType: %s", feedstockType)
                ));
    }

    private Predicate<Feedstock> matchesType(FeedstockType feedstockType) {
        return feedstock -> feedstock.type.equals(feedstockType);
    }

    private Predicate<Feedstock> matchesUnit(Unit unit) {
        return feedstock -> feedstock.unit == unit;
    }

    @Setter
    @NoArgsConstructor
    private static class Feedstock {
        private FeedstockType type;
        private Unit unit;
    }
}
