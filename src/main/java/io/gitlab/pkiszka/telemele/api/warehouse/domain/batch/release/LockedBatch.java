package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchRepository;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor(access = PRIVATE)
@Slf4j
@EqualsAndHashCode
class LockedBatch {

    private final BatchId batchId;

    static LockedBatch of(BatchId batchId) {
        return new LockedBatch(batchId);
    }

    ReleasedBatch release(OrderId orderId, BatchRepository batchRepository) {
        Batch batchToRelease = batchRepository.getById(batchId);
        Volume totalLockedVolumeBeforeReleasing = batchToRelease.getTotalLockedVolume(orderId);
        batchToRelease.release(orderId);
        Batch releasedBatch = batchRepository.saveBatch(batchToRelease);
        Volume totalLockedVolumeAfterReleasing = releasedBatch.getTotalLockedVolume(orderId);
        log.info("Released batchId: {}, orderId: {}", batchId, orderId);
        return ReleasedBatch.of(
                batchId,
                releasedBatch.getFeedstockType(),
                totalLockedVolumeBeforeReleasing.subtract(totalLockedVolumeAfterReleasing)
        );
    }
}
