package io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource;

import java.util.List;

import com.google.common.collect.ImmutableList;

public enum ResourceBookingStatus {
    NOT_PROCESSED,
    WAITING_FOR_AVAILABLE_RESOURCES,
    SETTLED,
    PARTIALLY_CANCELLED,
    CANCELLED;

    public static List<ResourceBookingStatus> unsettledOnly() {
        return ImmutableList.of(NOT_PROCESSED, WAITING_FOR_AVAILABLE_RESOURCES);
    }

    boolean isInCancellationProcess() {
        return this == CANCELLED || this == PARTIALLY_CANCELLED;
    }
}
