package io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit;

public interface Feedstocks {

    void verifyMatch(FeedstockType feedstockType, Unit unit);

    void verifyExists(FeedstockType feedstockType);
}
