package io.gitlab.pkiszka.telemele.api.warehouse.domain;


import java.io.Serializable;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.isNull;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Embeddable
@EqualsAndHashCode
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Setter(value = PRIVATE)
@Getter(value = PRIVATE)
public class OrderId implements Serializable { //ExternalOrderId?

    @JsonValue
    private String orderId;

    @JsonCreator
    public static OrderId of(String orderId) {
        checkArgument(!isNull(orderId), "Failed to instantiate OrderId, orderId value is null");
        checkArgument(!isBlank(orderId), "Failed to instantiate OrderId, orderId value is empty");
        return new OrderId(orderId);
    }

    @Override
    public String toString() {
        return this.orderId;
    }
}
