package io.gitlab.pkiszka.telemele.api.warehouse.infrastructure.persistance;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchRepository;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;

public interface JpaBatchRepository extends BatchRepository, JpaRepository<Batch, BatchId> {

    @Query("SELECT b FROM Batch b where b.feedstockType = :feedstockType AND b.status IN :batchStatuses")
    List<Batch> findBatches(@Param("feedstockType") FeedstockType feedstockType,
                            @Param("batchStatuses") List<BatchStatus> batchStatuses);


    @Query("SELECT b FROM Batch b where b.feedstockType = :feedstockType")
    List<Batch> findBatches(@Param("feedstockType") FeedstockType feedstockType);

    @Query("SELECT b FROM Batch b where b.status IN :batchStatuses")
    List<Batch> findBatches(@Param("batchStatuses") List<BatchStatus> batchStatuses);

    @Query("SELECT b FROM Batch b WHERE b.batchId = :batchId")
    Optional<Batch> findById(@Param("batchId") BatchId batchId);

    @Override
    default Batch getById(BatchId batchId) {
        return this.getOne(batchId);
    }

    @Override
    default Batch saveBatch(Batch batch) {
        return this.save(batch);
    }
}
