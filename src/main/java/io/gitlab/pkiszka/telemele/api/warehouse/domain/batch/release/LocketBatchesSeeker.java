package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release;

import java.util.Set;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableList;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchRepository;

import static io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus.AVAILABLE;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus.BLOCKED;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

@Service
public class LocketBatchesSeeker {

    public LockedBatches seekForLockedBatches(OrderId orderId, BatchRepository batchRepository) {
        requireNonNull(orderId);
        Set<BatchId> batchIds = batchRepository.findBatches(ImmutableList.of(AVAILABLE, BLOCKED)).stream()
                .filter(batch -> batch.getLockedVolumes().stream().anyMatch(lockedVolume -> lockedVolume.isOfOrderId(orderId)))
                .map(Batch::getBatchId)
                .collect(toSet());
        return LockedBatches.of(orderId, batchIds);
    }
}
