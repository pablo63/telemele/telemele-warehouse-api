package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;

import static java.lang.String.format;

class LockedBatchesException extends RuntimeException {

    private LockedBatchesException(String message, Object... formatArguments) {
        super(format(message, formatArguments));
    }

    static LockedBatchesException duplicatedBatchId(BatchId batchId) {
        return new LockedBatchesException(
                "Failed to add batchId to lockedBatches - batchId: '%s' already exists in LockedBatches",
                batchId
        );
    }
}
