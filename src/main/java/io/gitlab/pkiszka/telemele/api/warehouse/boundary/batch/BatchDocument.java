package io.gitlab.pkiszka.telemele.api.warehouse.boundary.batch;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import io.gitlab.pkiszka.telemele.api.warehouse.boundary.VolumeDocument;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batch;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import lombok.AllArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
class BatchDocument {

    @NotNull
    private BatchId batchId;

    @NotNull
    @Valid
    private VolumeDocument availableVolume;

    @NotNull
    private String feedstockType;

    public static BatchDocument from(Batch batch) {
        return new BatchDocument(
                batch.getBatchId(),
                VolumeDocument.from(batch.getAvailableVolume()),
                batch.getFeedstockType().asString()
        );
    }
}
