package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch;

import java.util.Set;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableSet;
import lombok.AllArgsConstructor;

import static java.lang.String.format;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
public enum BatchStatus {
    AVAILABLE("available"),
    BLOCKED("blocked"),
    UTILIZED("utilized");

    private final String name;

    public static BatchStatus from(String batchStatusName) {
        return Stream.of(BatchStatus.values())
                .filter(batchStatus -> batchStatus.name.equalsIgnoreCase(batchStatusName))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(
                        format("%s - batchStatus name is not defined", batchStatusName)
                ));
    }

    // todo - use it in place such as resource booking
    public static Set<BatchStatus> notUtilized() {
        return ImmutableSet.of(AVAILABLE, BLOCKED);
    }
}
