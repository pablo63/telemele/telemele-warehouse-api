package io.gitlab.pkiszka.telemele.api.warehouse.application.lockedResources;

import java.util.List;
import org.springframework.stereotype.Service;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchRepository;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBooking;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingRepository;
import lombok.AllArgsConstructor;

import static com.google.common.collect.ImmutableList.of;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus.AVAILABLE;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus.BLOCKED;
import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class LockedResourcesResolver {

    private BatchRepository batchRepository;
    private ResourceBookingRepository resourceBookingRepository;

    public List<LockedResource> getLockedResources(OrderId orderId) {
        return batchRepository.findBatches(of(AVAILABLE, BLOCKED)).stream()
                .filter(batch -> batch.hasLockedVolumesForOrderId(orderId))
                .map(batch -> {
                    ResourceBooking resourceBooking = resourceBookingRepository.getResourceBooking(batch.getFeedstockType(), orderId);
                    return LockedResource.builder()
                            .batchId(batch.getBatchId())
                            .feedstockType(batch.getFeedstockType())
                            .lockedVolumes(batch.getLockedVolumes().stream()
                                    .filter(lockedVolume -> lockedVolume.isOfOrderId(orderId))
                                    .collect(toList()))
                            .totalLockedVolume(resourceBooking.getTotalLockedVolume())
                            .volumeToLock(resourceBooking.getVolumeToLock())
                            .build();
                })
                .collect(toList());
    }

}
