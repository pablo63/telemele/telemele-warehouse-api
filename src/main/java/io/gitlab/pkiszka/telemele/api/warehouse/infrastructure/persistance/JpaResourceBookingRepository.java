package io.gitlab.pkiszka.telemele.api.warehouse.infrastructure.persistance;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBooking;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingRepository;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingStatus;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;

public interface JpaResourceBookingRepository extends ResourceBookingRepository, JpaRepository<ResourceBooking, ResourceBookingId> {

    @Query("SELECT l FROM ResourceBooking l WHERE l.status IN :resourceBookingStatuses")
    List<ResourceBooking> findResourceBookings(@Param("resourceBookingStatuses") List<ResourceBookingStatus> resourceBookingStatuses);

    @Query("SELECT l FROM ResourceBooking l WHERE l.orderId = :orderId")
    List<ResourceBooking> findResourceBookings(@Param("orderId") OrderId orderId);

    default ResourceBooking getResourceBooking(@Param("feedstockType") FeedstockType feedstockType,
                                               @Param("orderId") OrderId orderId) {
        return findAll().stream()
                .filter(lockOrder -> lockOrder.getOrderId().equals(orderId))
                .filter(lockOrder -> lockOrder.getFeedstockType().equals(feedstockType))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(""));
    }

    @Override
    default void saveOne(ResourceBooking resourceBooking) {
        save(resourceBooking);
    }

    @Override
    default void saveMany(List<ResourceBooking> resourceBookings) {
        saveAll(resourceBookings);
    }

    @Override
    Optional<ResourceBooking> findById(ResourceBookingId resourceBookingId);
}
