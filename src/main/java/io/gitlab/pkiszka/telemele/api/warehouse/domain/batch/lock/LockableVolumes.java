package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock;

import java.math.BigDecimal;
import java.util.List;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchRepository;
import lombok.AllArgsConstructor;

import static com.google.common.collect.Lists.newArrayList;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
public class LockableVolumes {

    private final OrderId orderId;
    private final List<LockableVolume> lockableVolumes;

    static LockableVolumes empty(OrderId orderId) {
        return new LockableVolumes(orderId, newArrayList());
    }

    void attachVolume(BatchId batchId, Volume volume) {
        if (batchIdAlreadyExists(batchId)) {
            throw LockableVolumesException.duplicatedBatchId(batchId);
        }
        LockableVolume lockableVolume = LockableVolume.of(batchId, volume);
        this.lockableVolumes.add(lockableVolume);
    }

    public void lockAll(BatchRepository batchRepository) {
        lockableVolumes.forEach(
                lockableVolume -> lockableVolume.lock(orderId, batchRepository)
        );
    }

    private boolean batchIdAlreadyExists(BatchId batchId) {
        return lockableVolumes.stream()
                .anyMatch(lockableVolume -> lockableVolume.hasBatchId(batchId));
    }

    BigDecimal getTotalVolumeValue() {
        return lockableVolumes.stream()
                .map(LockableVolume::getVolumeToLock)
                .map(Volume::getVolume)
                .reduce((BigDecimal::add))
                .orElse(BigDecimal.ZERO);
    }
}
