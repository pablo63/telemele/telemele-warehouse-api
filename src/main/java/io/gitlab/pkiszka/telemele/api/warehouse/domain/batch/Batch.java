package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch;

import java.util.Comparator;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock.LockedVolume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import static com.google.common.base.Preconditions.checkArgument;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus.AVAILABLE;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus.BLOCKED;
import static io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchStatus.UTILIZED;
import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static javax.persistence.EnumType.STRING;
import static lombok.AccessLevel.PRIVATE;

@Entity
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Getter
@ToString
public class Batch {

    @EmbeddedId
    private BatchId batchId;

    @AttributeOverrides({
            @AttributeOverride(name = "volume", column = @Column(name = "availableVolume"))
    })
    private Volume availableVolume;

    @ElementCollection(targetClass = LockedVolume.class)
    @CollectionTable(name = "locked_volume", joinColumns = @JoinColumn(name = "batch_id"))
    private List<LockedVolume> lockedVolumes;

    @Enumerated(STRING)
    private BatchStatus status;

    @Embedded
    private FeedstockType feedstockType;

    public static Batch of(Volume availableVolume, FeedstockType feedstockType) {
        checkArgument(!isNull(availableVolume), "Cannot instantiate Batch - availableVolume is null");
        checkArgument(!isNull(feedstockType), "Cannot instantiate Batch - feedstockType is null");
        return new Batch(
                BatchId.generate(),
                availableVolume,
                emptyList(),
                BatchStatus.AVAILABLE,
                feedstockType
        );
    }

    public Volume getTotalLockedVolume(OrderId orderId) {
        return lockedVolumes.stream()
                .filter(lockedVolume -> lockedVolume.isOfOrderId(orderId))
                .map(LockedVolume::getVolume)
                .reduce(Volume.ZERO, Volume::add);
    }

    public boolean hasLockedVolumesForOrderId(OrderId orderId) {
        return lockedVolumes.stream()
                .anyMatch(lockedVolume -> lockedVolume.isOfOrderId(orderId));
    }

    public void lock(Volume volumeToLock, OrderId orderId) {
        if (volumeToLock.isGreaterThan(availableVolume)) {
            throw BatchException.insufficientAvailableVolume(batchId, orderId, volumeToLock);
        }
        executeLockingVolume(volumeToLock, orderId);
        determineBatchStatus();
    }

    public void release(OrderId orderId) {
        executeReleasingVolumes(orderId);
        determineBatchStatus();
    }

    private void executeLockingVolume(Volume volumeToLock, OrderId orderId) {
        LockedVolume lockedVolume = LockedVolume.of(orderId, volumeToLock);
        this.lockedVolumes.add(lockedVolume);
        this.availableVolume = availableVolume.subtract(lockedVolume.getVolume());
    }

    private void executeReleasingVolumes(OrderId orderId) {
        List<LockedVolume> volumesToUnlock = lockedVolumes.stream()
                .filter(lockedVolume -> lockedVolume.isOfOrderId(orderId))
                .collect(toList());
        Volume totalVolumeToUnlock = volumesToUnlock.stream()
                .map(LockedVolume::getVolume)
                .reduce(Volume::add)
                .orElseThrow(() -> BatchException.lockedVolumesForOrderIdNotFound(batchId, orderId));
        this.lockedVolumes.removeAll(volumesToUnlock);
        this.availableVolume = availableVolume.add(totalVolumeToUnlock);
    }

    private void determineBatchStatus() {
        if (availableVolume.isEmpty()) {
            if (lockedVolumes.isEmpty()) {
                status = UTILIZED;
            } else {
                status = BLOCKED;
            }
        } else {
            status = AVAILABLE;
        }
    }

    public static class AvailableVolumeComparator implements Comparator<Batch> {
        @Override
        public int compare(Batch batch1, Batch batch2) {
            checkArgument(!isNull(batch1), "cannot compare batches - first argument (batch1) is null");
            checkArgument(!isNull(batch2), "cannot compare batches - second argument (batch2) is null");
            return batch1.getAvailableVolume().getVolume().compareTo(batch2.getAvailableVolume().getVolume());
        }
    }
}
