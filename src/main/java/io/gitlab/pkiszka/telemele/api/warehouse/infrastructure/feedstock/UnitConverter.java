package io.gitlab.pkiszka.telemele.api.warehouse.infrastructure.feedstock;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.Unit;

@Component
@ConfigurationPropertiesBinding
public class UnitConverter implements Converter<String, Unit> {

    @Override
    public Unit convert(String unitName) {
        return Unit.from(unitName);
    }
}
