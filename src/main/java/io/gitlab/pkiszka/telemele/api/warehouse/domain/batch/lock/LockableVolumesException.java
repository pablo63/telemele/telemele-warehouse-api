package io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.BatchId;

import static java.lang.String.format;

class LockableVolumesException extends RuntimeException {

    private LockableVolumesException(String message, Object... formatArguments) {
        super(format(message, formatArguments));
    }

    static LockableVolumesException duplicatedBatchId(BatchId batchId) {
        return new LockableVolumesException(
                "Failed to attach volume to lock - batchId: '%s' already exists in LockableVolumes",
                batchId
        );
    }
}

