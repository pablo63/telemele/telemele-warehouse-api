package io.gitlab.pkiszka.telemele.api.warehouse.application.resourcesBooking;

import java.util.List;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.Volume;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.Batches;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.lock.LockVolumeAttempt;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.batch.release.ReleasedBatches;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBooking;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookingRepository;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBookings;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.Feedstocks;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Component
@Slf4j
@Transactional
public class ResourcesBookingSaga {

    private final ApplicationEventPublisher publisher;
    private final ResourceBookingRepository resourceBookingRepository;
    private final Feedstocks feedstocks;
    private final Batches batches;
    private final ResourceBookings resourceBookings;

    public void acceptNewResourcesToBook(List<ResourceBooking> resourceBookings) {
        resourceBookings.forEach(resourceBooking -> feedstocks.verifyMatch(resourceBooking.getFeedstockType(), resourceBooking.getUnit()));
        resourceBookingRepository.saveMany(resourceBookings);
        publisher.publishEvent(new ProcessUnsettledResourceBookingsEvent());
    }

    public void releaseBookedResources(OrderId orderId) {
        resourceBookings.verifyBookingsExistFor(orderId);
        ReleasedBatches releasedBatches = batches.releaseLockedVolumes(orderId);
        releasedBatches.getReleasedBatches().forEach(
                releasedBatch -> resourceBookings.updateStatusAfterReleasingLock(
                        orderId, releasedBatch.getFeedstockType(), releasedBatch.getReleasedVolume()
                )
        );
        publisher.publishEvent(new ProcessUnsettledResourceBookingsEvent());
    }

    public void processUnsettledBookings() {
        resourceBookings.getUnsettledResourceBookings().forEach(
                resourceBooking -> lockVolume(resourceBooking.getRemainingVolumeToBook(), resourceBooking.getFeedstockType(), resourceBooking.getOrderId())
        );
    }

    private void lockVolume(Volume volumeToLock, FeedstockType feedstockType, OrderId orderId) {
        LockVolumeAttempt lockVolumeAttempt = LockVolumeAttempt.of(volumeToLock, feedstockType, orderId);
        batches.attemptLockOn(lockVolumeAttempt);
        Volume totalLockedVolume = batches.getTotalLockedVolume(feedstockType, orderId);
        resourceBookings.updateStatusAfterPerformingLock(orderId, feedstockType, totalLockedVolume);
    }
}
