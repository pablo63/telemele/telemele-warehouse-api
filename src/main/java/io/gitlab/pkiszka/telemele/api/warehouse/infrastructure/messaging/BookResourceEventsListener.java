package io.gitlab.pkiszka.telemele.api.warehouse.infrastructure.messaging;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import io.gitlab.pkiszka.telemele.api.warehouse.application.resourcesBooking.ProcessUnsettledResourceBookingsEvent;
import io.gitlab.pkiszka.telemele.api.warehouse.application.resourcesBooking.ResourcesBookingSaga;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@AllArgsConstructor
@Slf4j
public class BookResourceEventsListener {

    private final ResourcesBookingSaga resourcesBookingSaga;

    @Async
    @EventListener
    void handle(ProcessUnsettledResourceBookingsEvent processUnsettledResourceBookingsEvent) {
        resourcesBookingSaga.processUnsettledBookings();
    }
}
