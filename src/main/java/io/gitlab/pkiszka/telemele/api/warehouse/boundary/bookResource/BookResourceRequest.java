package io.gitlab.pkiszka.telemele.api.warehouse.boundary.bookResource;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.gitlab.pkiszka.telemele.api.warehouse.boundary.VolumeDocument;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.OrderId;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.bookResource.ResourceBooking;
import io.gitlab.pkiszka.telemele.api.warehouse.domain.feedstock.FeedstockType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@Getter
class BookResourceRequest {

    @Size(min = 1)
    @NotNull
    private List<@Valid VolumeToLockDocument> volumes;

    List<ResourceBooking> asLockVolumeRequest(OrderId orderId) {
        return volumes.stream()
                .map(volumeToLockDocument -> ResourceBooking.from(
                        volumeToLockDocument.volume.asVolume(),
                        FeedstockType.of(volumeToLockDocument.feedstockType),
                        orderId
                ))
                .collect(toList());
    }

    @AllArgsConstructor(access = PRIVATE)
    @Getter
    @Setter
    private static class VolumeToLockDocument {
        @NotNull
        @Valid
        //should not be negative!!!
        private VolumeDocument volume;

        @NotNull
        // should not be duplicated
        private String feedstockType;
    }
}
